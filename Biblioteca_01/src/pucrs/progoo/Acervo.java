package pucrs.progoo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Acervo {

	private ArrayList<Obra> acervo;

	public Acervo() {
		acervo = new ArrayList<Obra>();
	}
	
	public void ordenaTitulo() {
//        Collections.sort(acervo);
//		acervo.sort( (Livro o1, Livro o2) -> o1.getTitulo().compareTo(o2.getTitulo()));
//		acervo.sort(Comparator.comparing(livro -> livro.getTitulo()));
		acervo.sort(Comparator.comparing(Obra::getTitulo).thenComparing(Obra::getAno));
    }
	
	public void ordenaCodigo() {
//        acervo.sort(Comparator.comparing((Livro livro) -> livro.getCodigo()).reversed());
        acervo.sort(Comparator.comparing(Obra::getCodigo).reversed());
    }
	
	public boolean cadastrarObra(Obra novo) {
		// Verifica se o código da obra já está no acervo (ou seja, se a obra já existe)
		if(buscarPorCodigo(novo.getCodigo()) != null)
				return false;
		acervo.add(novo);
		return true;
	}
	
	public String listarTodos() {
		StringBuilder sb = new StringBuilder();
		for(Obra obra: acervo)
			sb.append(obra+"\n");
		return sb.toString();
	}

	public Obra buscarPorCodigo(int codigo) {
		for(Obra obra: acervo)
			if(obra.getCodigo() == codigo)
				return obra;
		return null;
	}
	
	public Obra buscarPorTitulo(String titulo) {
		for(Obra obra: acervo)
			if(obra.getTitulo().equals(titulo))
				return obra;
		return null;
	}
	
	/*
	public ArrayList<Livro> buscarPorAutor(String autor) {
		ArrayList<Livro> novo = new ArrayList<>();
		for(Livro livro: acervo)
			if(livro.getAutor().equals(autor))
				novo.add(livro);
		return novo;	
	}
	*/
}